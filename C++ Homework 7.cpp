﻿#include <iostream>
using namespace std;

class Animals
{
public:
    virtual void Voice()
    {
        cout << "Voice\n";
    };
};

class Dog : public Animals
{
public:
    void Voice() override
    {
        cout << "!woof\n";
    }
};

class Cat : public Animals
{
public:
    void Voice() override
    {
        cout << "!meow\n";
    }
};

class Cow : public Animals
{
public:
    void Voice() override
    {
        cout << "!moo\n";
    }
};

int main()
{
    const int a =3;

    Animals* animals [a] = {new Cat, new Dog, new Cow};
    
    for (int i = 0; i < a; i++)
    {
        animals[i]->Voice();
    }
};